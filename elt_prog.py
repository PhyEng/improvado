import sys
import csv
import json
import collections
import xml.etree.ElementTree as ET

# Функция сортировки csv файлов, возвращает отсортированные массив
def sort_csv(matrix):
    for i in range(len(matrix[0]) - 1):
        while matrix[0][i] > matrix[0][i + 1] and i >= 0:
            matrix[0][i], matrix[0][i + 1] = matrix[0][i + 1], matrix[0][i]
            for j in range(len(matrix) - 1):
                matrix[j + 1][i], matrix[j + 1][i + 1] \
                    = matrix[j + 1][i + 1], matrix[j + 1][i]
            i -= 1
    return matrix


# Считываем файлы от пользователя
csv_fp_1 = str(input('>>> Enter first csv file name: '))
csv_fp_2 = str(input('>>> Enter second csv file name: '))
xml_fp = str(input('>>> Enter xml file name: '))
json_fp = str(input('>>> Enter json file name: '))
print()

# Считываем данные cvs файлов
try:
    with open(csv_fp_1, 'r') as fp1, \
            open(csv_fp_2, 'r') as fp2:
        data_csv1 = csv.reader(fp1)
        data_csv2 = csv.reader(fp2)
        array_total = sort_csv([row for row in data_csv1])
        array_csv_2 = sort_csv([row for row in data_csv2])
except FileNotFoundError as e:
    print(e)
    sys.exit(1)

# Обьединяем csv файлы в общий массив
for i in range(len(array_total) - 1):
    inter_list = []
    z = 0
    for j in range(len(array_total[0]) + 1):
        if array_total[0][z] == array_csv_2[0][j]:
            inter_list.append(array_csv_2[i + 1][j])
            z += 1
    array_total.append(inter_list)

# Парсим xml файл
value = {}
root = ET.parse(xml_fp).getroot()

for type_tag in root.findall('objects/object'):
    value[type_tag.attrib['name']] = type_tag.find('value').text
# Добавляем все значения из xml в словарь для последующей сортировки
sorted_value = collections.OrderedDict(sorted(value.items()))
sorted_value_list = []
for k, v in sorted_value.items():
    sorted_value_list.append(v)

# Добавляем в общий массив
array_total.append(sorted_value_list)

# Считываем данные с json
with open(json_fp, 'r') as read_file:
    json_data = json.load(read_file)
    for row in json_data['fields']:
        json_arr = []
        # Сортируем столбцы Dn Mn и добавляем в общий массив 
        key = sorted(row)
        for value in range(0, len(array_total[0])):
            json_arr.append(str(row[key[value]]))
        array_total.append(json_arr)

# Вычисляем количество Dn столбцов для последующих вычислений
count_d = 0
for str_d in array_total[0]:
    if str_d[0] == 'D':
        count_d += 1

# Преобразуем строковые числа в int тип и проверяем на корректнсть данных
total_data = []
total_data.append(array_total[0])

try:
    for i in range(1, len(array_total)):
        value = array_total[i][0:count_d]
        value += list(map(int, array_total[i][count_d:]))
        total_data.append(value)
except ValueError:
    print('>>> Invalid data values ​​in files.')

# Общий массив со всеми файлами отсортированный по D1
total_data.sort(key=lambda a: a[0])

# Запись в данных basic_results.tsv файл
with open('basic_results.tsv', 'w', newline='') as basic_res:
    writer = csv.writer(basic_res, delimiter='\t')
    writer.writerows(total_data)

print('>>> Writing data to file "basic_results.tsv" completed successfully.')

# Advanced part
# Переименнование Mn столбцов в MSn
for i in range(len(total_data[0])):
    word = []
    if total_data[0][i][0] == 'M':
        text = total_data[0][i][1]
        total_data[0].pop(i)
        total_data[0].insert(i, 'MS' + text)

#Вычисление суммы значений соответствующих **M1**...**Mn** из 4 файлов
i = 1
while i < len(total_data) - 1:
    z = i + 1
    while z <= (len(total_data) - 1):
        value = total_data[i][0:count_d]
        if value[:count_d] == total_data[z][0:count_d]:
            # Считываем числа из Mn столбцов
            numbers_1 = total_data[i][count_d:]
            numbers_2 = total_data[z][count_d:]
            # Вычисляем MSn значения
            for index in range(len(numbers_1)):
                numbers_1[index] += numbers_2[index]
            value += numbers_1
            total_data.pop(i)
            total_data.pop(z - 1)
            total_data.insert(i, value)
            z -= 1
        z += 1
    i += 1

# Отсорируем данные
total_data.sort()

# Записываем итоговый массив в advanced_results.tsv
with open('advanced_results.tsv', 'w', newline='') as basic_res:
    writer = csv.writer(basic_res, delimiter='\t')
    writer.writerows(total_data)

print('>>> Writing data to file "advanced_results.tsv" completed successfully.')
